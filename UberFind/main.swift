//
//  main.swift
//  UberFind
//
//  Created by Vladislav Alexeev on 15.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation
import FinderLibrary

class Main: FinderDelegate {
    func run() -> Int32 {
        let argumentsParser = ArgumentsParser(arguments: ProcessInfo.processInfo.arguments)
        do {
            let config = try argumentsParser.parse()
            let finder = Finder(runConfig: config, delegate: self)
            finder.find()
            return 0
        } catch let error {
            if let userFriendlyError = error as? UserFriendlyError {
                print(userFriendlyError.humanReadableDescription)
            } else {
                print("Error parsing arguments: \(error)")
            }
            return 1
        }
    }

    func didFindPath(_ path: String) {
        print("-> \(path)")
    }
}

func main() -> Int32 {
    return Main().run()
}

exit(main())
