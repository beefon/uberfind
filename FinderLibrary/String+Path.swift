//
//  String+Path.swift
//  Finder
//
//  Created by Vladislav Alexeev on 17.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

public extension String {
    var pathComponents: [String] {
        return (self as NSString).pathComponents
    }

    static func pathWithComponents(components: [String]) -> String {
        return NSString.path(withComponents: components)
    }

    func isRelativePath() -> Bool {
        var relativePathPrefixSet = CharacterSet.alphanumerics
        relativePathPrefixSet.insert(charactersIn: ".")

        if let prefix = first, let firstUnicode = prefix.unicodeScalars.first {
            return relativePathPrefixSet.contains(firstUnicode)
        }
        return false
    }

    func dotPrefix() -> String? {
        guard isRelativePath() else { return nil }
        if self == "." || self == ".." { return self }
        guard let index = self.index(of: Character("/")) else { return nil }
        let beforeSlash = String(prefix(upTo: index))
        let notDotCharacterSet = CharacterSet(charactersIn: ".").inverted
        if beforeSlash.rangeOfCharacter(from: notDotCharacterSet) != nil {
            return nil
        }
        return beforeSlash
    }

    func stringWithPathRelativeTo(anchorPath: String) -> String? {
        let pathComponents = self.pathComponents
        let anchorComponents = anchorPath.pathComponents

        var componentsInCommon = 0
        for (c1, c2) in zip(pathComponents, anchorComponents) {
            if c1 != c2 {
                break
            }
            componentsInCommon += 1
        }

        let numberOfParentComponents = anchorComponents.count - componentsInCommon
        let numberOfPathComponents = pathComponents.count - componentsInCommon

        var relativeComponents = [String]()
        relativeComponents.reserveCapacity(numberOfParentComponents + numberOfPathComponents)
        for _ in 0..<numberOfParentComponents {
            relativeComponents.append("..")
        }
        relativeComponents.append(contentsOf: pathComponents[componentsInCommon..<pathComponents.count])

        let url = NSURL.fileURL(withPathComponents: relativeComponents)
        return url?.relativePath
    }
}
