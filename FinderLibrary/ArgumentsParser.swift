//
//  Argumentz.swift
//  Finder
//
//  Created by Vladislav Alexeev on 17.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

public struct ArgumentsParser {
    public enum ParseError: Error, Equatable, UserFriendlyError {
        public static func ==(left: ArgumentsParser.ParseError, right: ArgumentsParser.ParseError) -> Bool {
            switch (left, right) {
            case (.moreThanSinglePathGiven, .moreThanSinglePathGiven):
                return true
            case (.pathMissing, .pathMissing):
                return true
            case (.unknownArgument(let leftArg), .unknownArgument(let rightArg)):
                return leftArg == rightArg
            case (.noArgument(let leftIndex), .noArgument(let rightIndex)):
                return leftIndex == rightIndex
            default:
                return false
            }
        }

        case moreThanSinglePathGiven
        case pathMissing
        case noArgument(atIndex: Int)
        case unknownArgument(argument: String)

        public var humanReadableDescription: String {
            switch self {
            case .moreThanSinglePathGiven:
                return "More than single path was given"
            case .pathMissing:
                return "You must specify path"
            case .noArgument(let index):
                return "Expected to find argument at index \(index), but no argument was given."
            case .unknownArgument(let argument):
                return "Unknown argument: \(argument)."
            }
        }
    }

    private enum ArgumentKind {
        case symlinkOption
        case excludePath
        case expression
    }

    private struct ArgumentValue {
        let kind: ArgumentKind
        let name: String
        let value: String?
    }

    private let arguments: [String]

    public init(arguments: [String]) {
        self.arguments = arguments
    }

    public func parse() throws -> RunConfig {
        var argumentIndex = 1
        var argumentKind = [Int: ArgumentValue]()

        while argumentIndex < arguments.count {
            let name = try argument(atIndex: argumentIndex)
            if name == "-P" || name == "-L" {
                argumentKind[argumentIndex] = ArgumentValue(kind: .symlinkOption, name: name, value: nil)
            } else if name == "--exclude-dir" {
                let value = try argument(atIndex: argumentIndex + 1)
                argumentKind[argumentIndex] = ArgumentValue(kind: .excludePath, name: name, value: value)
                argumentIndex += 1
            } else if name == "-type" || name == "-name" || name == "-rname"  {
                let value = try argument(atIndex: argumentIndex + 1)
                argumentKind[argumentIndex] = ArgumentValue(kind: .expression, name: name, value: value)
                argumentIndex += 1
            }
            argumentIndex += 1
        }

        return try createRunConfigFromArguments(argumentKind)
    }

    private func argument(atIndex index: Int) throws -> String {
        guard index < arguments.count else {
            throw ParseError.noArgument(atIndex: index)
        }
        return arguments[index]
    }

    private func createRunConfigFromArguments(_ argumentKind: [Int: ArgumentValue]) throws -> RunConfig {
        var acceptedIndexes = IndexSet(integer: 0)

        let runConfigBuilder = RunConfigBuilder()

        try argumentKind.forEach { (index: Int, value: ArgumentValue) in
            acceptedIndexes.insert(index)

            switch value.kind {
            case .symlinkOption:
                runConfigBuilder.with(symbolicLinkBehaviour: SymbolicLinkBehaviour(rawValue: value.name)!)
            case .excludePath:
                acceptedIndexes.insert(index + 1)
                runConfigBuilder.with(pathToExclude: value.value)
            case .expression:
                acceptedIndexes.insert(index + 1)
                switch value.name {
                case "-type":
                    guard let rawValue = value.value, let fileType = Expression.FileType(rawValue: rawValue) else {
                        throw ParseError.unknownArgument(argument: value.name)
                    }
                    runConfigBuilder.with(mergedExpression: Expression(fileType: fileType))
                case "-name":
                    runConfigBuilder.with(mergedExpression: Expression(name: value.value))
                case "-rname":
                    runConfigBuilder.with(mergedExpression: Expression(rname: value.value))
                default:
                    try argumentIsMissingProcessing(value)
                }
            }
        }

        let allIndexes = IndexSet(integersIn: 0 ..< arguments.count)
        let unparsedIndexes = allIndexes.subtracting(acceptedIndexes)

        guard unparsedIndexes.count <= 1 else {
            throw ParseError.moreThanSinglePathGiven
        }
        guard let pathArgumentIndex = unparsedIndexes.first else {
            throw ParseError.pathMissing
        }

        let path = arguments[pathArgumentIndex]
        runConfigBuilder.with(path: path)

        return try runConfigBuilder.build()
    }

    private func argumentIsMissingProcessing(_ argument: ArgumentValue) throws {
        throw ParseError.unknownArgument(argument: argument.name)
    }
}
