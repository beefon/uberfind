//
//  Expression.swift
//  Finder
//
//  Created by Vladislav Alexeev on 24.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

public struct Expression: Equatable {
    public enum FileType: String {
        case directory = "d"
        case regularFile = "f"
        case symbolicLink = "l"
    }

    public let fileType: FileType?
    public let name: String?
    public let rname: String?

    public init(fileType: FileType?,
                name: String?,
                rname: String?) {
        self.fileType = fileType
        self.name = name
        self.rname = rname
    }

    public init(fileType: FileType?) {
        self.init(fileType: fileType, name: nil, rname: nil)
    }

    public init(name: String?) {
        self.init(fileType: nil, name: name, rname: nil)
    }

    public init(rname: String?) {
        self.init(fileType: nil, name: nil, rname: rname)
    }

    public init() {
        self.init(fileType: nil, name: nil, rname: nil)
    }

    public static func ==(left: Expression, right: Expression) -> Bool {
        return left.fileType == right.fileType
            && left.name == right.name
            && left.rname == right.rname
    }
}
