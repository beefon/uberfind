//
//  FileTypePredicate.swift
//  Finder
//
//  Created by Vladislav Alexeev on 24.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

class FileTypePredicate: FilePredicate {
    private let defaultEvaluationOnError = false
    private let fileType: Expression.FileType

    public init(fileType: Expression.FileType) {
        self.fileType = fileType
    }

    public func evaluate(url: URL) -> Bool {
        let resourceKeys: Set<URLResourceKey> = Set([fileType.urlResourceKey])
        guard let resourceValues = try? url.resourceValues(forKeys: resourceKeys),
        let isUrlValid = fileType.boolValue(fromURLResourceValues: resourceValues) else {
            return defaultEvaluationOnError
        }
        return isUrlValid
    }
}

extension Expression.FileType {
    var urlResourceKey: URLResourceKey {
        switch self {
        case .directory:
            return URLResourceKey.isDirectoryKey
        case .regularFile:
            return URLResourceKey.isRegularFileKey
        case .symbolicLink:
            return URLResourceKey.isSymbolicLinkKey
        }
    }

    func boolValue(fromURLResourceValues urlResourceValues: URLResourceValues) -> Bool? {
        switch self {
        case .directory:
            return urlResourceValues.isDirectory
        case .regularFile:
            return urlResourceValues.isRegularFile
        case .symbolicLink:
            return urlResourceValues.isSymbolicLink
        }
    }
}
