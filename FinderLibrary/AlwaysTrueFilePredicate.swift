//
//  AlwaysTrueFilePredicate.swift
//  Finder
//
//  Created by Vladislav Alexeev on 26.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

class AlwaysTrueFilePredicate: FilePredicate {
    func evaluate(url: URL) -> Bool {
        return true
    }
}
