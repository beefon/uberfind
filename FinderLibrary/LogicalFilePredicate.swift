//
//  LogicalFilePredicate.swift
//  Finder
//
//  Created by Vladislav Alexeev on 24.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

class LogicalFilePredicate: FilePredicate {
    private let predicates: [FilePredicate]

    init(predicates: [FilePredicate]) {
        self.predicates = predicates
    }

    func evaluate(url: URL) -> Bool {
        for filePredicate in predicates {
            if !filePredicate.evaluate(url: url) { return false }
        }
        return true
    }
}
