//
//  FileNamePredicate.swift
//  Finder
//
//  Created by Vladislav Alexeev on 24.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

class FileNamePredicate: FilePredicate {
    private let fileName: String

    init(fileName: String) {
        self.fileName = fileName
    }

    func evaluate(url: URL) -> Bool {
        return fileName == url.lastPathComponent
    }
}
