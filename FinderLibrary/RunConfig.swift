//
//  RunConfig.swift
//  UberFind
//
//  Created by Vladislav Alexeev on 15.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

public enum SymbolicLinkBehaviour: String {
    case doNotFollow = "-P"
    case follow = "-L"
}

public struct RunConfig {
    public let symbolicLinkBehaviour: SymbolicLinkBehaviour
    public let path: String
    public let pathToExclude: String?
    public let predicate: FilePredicate

    fileprivate init(
        symbolicLinkBehaviour: SymbolicLinkBehaviour = .doNotFollow,
        pathToExclude: String? = nil,
        path: String,
        predicate: FilePredicate)
    {
        self.symbolicLinkBehaviour = symbolicLinkBehaviour
        self.pathToExclude = pathToExclude
        self.path = path
        self.predicate = predicate
    }

    public func shouldOutputRelativePath() -> Bool {
        return path.isRelativePath()
    }
}

public class RunConfigBuilder {
    public enum BuildError: String, Error, UserFriendlyError {
        case pathIsMissingOrEmpty = "You must specify non-empty path"
        case expressionHasEmptyNameValue = "You must specify non-empty -name"
        case expressionHasEmptyRnameValue = "You must specify non-empty -rname"
        case invalidRegularExpression = "Provided regular expression is invalid, it cannot be compiled"

        public var humanReadableDescription: String {
            return self.rawValue
        }
    }

    private var symbolicLinkBehaviour: SymbolicLinkBehaviour = .doNotFollow
    private var pathToExclude: String? = nil
    private var path: String? = nil
    private var expression: Expression = Expression()
    private var predicate: FilePredicate = AlwaysTrueFilePredicate()

    public convenience init(path: String) {
        self.init()
        self.path = path
    }

    @discardableResult
    public func with(symbolicLinkBehaviour: SymbolicLinkBehaviour) -> RunConfigBuilder {
        self.symbolicLinkBehaviour = symbolicLinkBehaviour
        return self
    }

    @discardableResult
    public func with(pathToExclude: String?) -> RunConfigBuilder {
        self.pathToExclude = pathToExclude
        return self
    }

    @discardableResult
    public func with(path: String) -> RunConfigBuilder {
        self.path = path
        return self
    }

    @discardableResult
    public func with(mergedExpression expression: Expression) -> RunConfigBuilder {
        let existingExpression = self.expression
        let newExpression = Expression(
            fileType: expression.fileType ?? existingExpression.fileType,
            name: expression.name ?? existingExpression.name,
            rname: expression.rname ?? existingExpression.rname
        )
        self.expression = newExpression
        return self
    }

    private func buildPredicate() throws -> FilePredicate {
        var predicates = [FilePredicate]([AlwaysTrueFilePredicate()])

        if let fileType = expression.fileType {
            predicates.append(FileTypePredicate(fileType: fileType))
        }
        if let filename = expression.name {
            predicates.append(FileNamePredicate(fileName: filename))
        }
        if let pattern = expression.rname {
            guard let patternPredicate = FileNamePatternPredicate(pattern: pattern) else {
                throw BuildError.invalidRegularExpression
            }
            predicates.append(patternPredicate)
        }
        let predicate = LogicalFilePredicate(predicates: predicates)
        return predicate
    }

    public func build() throws -> RunConfig {
        guard let path = path, !path.isEmpty else {
            throw BuildError.pathIsMissingOrEmpty
        }
        if let fileName = expression.name, fileName.count == 0 {
            throw BuildError.expressionHasEmptyNameValue
        }
        if let pattern = expression.rname, pattern.count == 0 {
            throw BuildError.expressionHasEmptyRnameValue
        }
        return RunConfig(
            symbolicLinkBehaviour: symbolicLinkBehaviour,
            pathToExclude: pathToExclude,
            path: path,
            predicate: try buildPredicate()
        )
    }
}
