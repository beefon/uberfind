//
//  Finder.swift
//  UberFind
//
//  Created by Vladislav Alexeev on 15.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

public class Finder {
    private let runConfig: RunConfig
    private weak var delegate: FinderDelegate?

    public init(runConfig: RunConfig, delegate: FinderDelegate) {
        self.runConfig = runConfig
        self.delegate = delegate
    }

    public func find() {
        let fileManager = FileManager()
        guard let enumerator = fileManager.enumerator(atPath: runConfig.path) else { return }
        let baseURL = URL(fileURLWithPath: runConfig.path)

        while let path = enumerator.nextObject() as? String {
            let initialUrl = URL(fileURLWithPath: path, relativeTo: baseURL)
            let url = resolve(url: initialUrl, baseURL: baseURL)
            if shouldExclude(url: url, baseURL: baseURL) {
                enumerator.skipDescendants()
            } else if shouldProcess(url: url) {
                let output = prepareURLForOutput(url)
                delegate?.didFindPath(output)
            }
        }
    }

    private func shouldProcess(url: URL) -> Bool {
        return runConfig.predicate.evaluate(url: url)
    }

    private func shouldExclude(url: URL, baseURL: URL) -> Bool {
        guard let pathToExclude = runConfig.pathToExclude,
            let relativePath = pathToExclude.stringWithPathRelativeTo(anchorPath: runConfig.path)
            else { return false }

        let urlToExclude = URL(fileURLWithPath: relativePath, relativeTo: baseURL)
        return url == urlToExclude
    }

    private func resolve(url: URL, baseURL: URL) -> URL {
        guard runConfig.symbolicLinkBehaviour == .follow else { return url }
        guard let values = try? url.resourceValues(forKeys: Set([.isSymbolicLinkKey])),
            values.isSymbolicLink == true else { return url }
        let fileManager = FileManager.default
        guard let destinationPath = try? fileManager.destinationOfSymbolicLink(atPath: url.path) else { return url }
        let resolvedUrl = URL(fileURLWithPath: destinationPath, relativeTo: baseURL)
        if fileManager.fileExists(atPath: resolvedUrl.path) {
            return resolvedUrl
        } else {
            return url
        }
    }

    private func prepareURLForOutput(_ url: URL) -> String {
        if runConfig.shouldOutputRelativePath() {
            let relativePath = runConfig.path + "/" + url.relativePath
            return relativePath
        } else {
            return url.path
        }
    }
 }
