//
//  FilePredicte.swift
//  Finder
//
//  Created by Vladislav Alexeev on 24.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

public protocol FilePredicate {
    func evaluate(url: URL) -> Bool
}
