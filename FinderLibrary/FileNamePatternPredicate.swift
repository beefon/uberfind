//
//  FileNameRegexPredicate.swift
//  Finder
//
//  Created by Vladislav Alexeev on 24.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

class FileNamePatternPredicate: FilePredicate {
    private let expression: NSRegularExpression

    init?(pattern: String) {
        guard let expression = try? NSRegularExpression(pattern: pattern, options: []) else {
            return nil
        }
        self.expression = expression
    }

    func evaluate(url: URL) -> Bool {
        let filename = url.lastPathComponent
        let match = expression.firstMatch(in: filename, options: [], range: NSRange(location: 0, length: filename.count))
        return match != nil
    }
}
