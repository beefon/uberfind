//
//  FinderDelegate.swift
//  Finder
//
//  Created by Vladislav Alexeev on 26.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

public protocol FinderDelegate: class {
    func didFindPath(_ path: String)
}
