#  Implementation details

This project uses mostly Swift standart libraries. It uses some NSString and NSURL macOS Foundation methods to simplfy works related to file system paths. They can be re-implemented in project, making this project dependable only on Swift libraries.
I can't tell if this project will be buildable on pure Darwin/Linux even if you get rid from dependency on Foundation.

There is a number of tests that cover some use cases.

The main aspects I paid attention to are:

* `find` streams events as soon as it locates a matching file on disk. I've implemented a simple streaming interface via `FinderDelegate`.
* `find` outputs relative or absolute paths depending on where you've asked it to perform search. I've concluded that outputs absosule paths if the `path` argument does not start with `.` or alphanumeric symbol. I've implemented this behavoir, but there could be more complex condition inside the `find` tool.
* It is possible to combine `-name` and `-rname` together. Although it does not make sense at first look, for simplicity I've left it as is. Perhaps all I need to do is make it `enum` instead of fields of the `struct`.
* The position of arguments in `expression` makes no difference. E.g. if you type `-type d -rname .*\.bundle` it will operate the same as if you swap the arguments: `-rname .*\.bundle -type d`.
* Only single `-type`, `-name`, `-rname` are supported.
* Only single `--exclude-dir` is supported.

The main logic is part of `libFinderLibrary` static library, as it is easier to test it from unit tests. It is possible to make integration tests that will test executable binary but with some other tooling, e.g. python test framework. I haven't done this.

# Testing

You can run tests from Xcode project: open it and hit Test (cmd+U).

# Building

Run `xcodebuild` from prject root, which will put the compiled binary into `build/Release/UberFind` relative to project's root folder.

# Running

You can add `build/Release/UberFind` to your `PATH` env and run the `UberFind` from terminal/shell script.
Alternatively, you can always use absolute path to the `UberFind` binary.
