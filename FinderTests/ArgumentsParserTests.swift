//
//  ArgumentsParserTests.swift
//  FinderTests
//
//  Created by Vladislav Alexeev on 20.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation
import XCTest
@testable import FinderLibrary

class ArgumentsParserTests: XCTestCase {
    func testValidShortestArguments() {
        let arguments: [String] = [
            "/path/to/uberfind",
            "where/to/perform/search"
        ]
        let parser = ArgumentsParser(arguments: arguments)
        let runConfig = try! parser.parse()
        XCTAssertEqual(runConfig.path, "where/to/perform/search")
        XCTAssertEqual(runConfig.symbolicLinkBehaviour, .doNotFollow)
        XCTAssertEqual(runConfig.pathToExclude, nil)
    }

    func testExplicitDoNotFollowSymlinks() {
        let arguments: [String] = [
            "/path/to/uberfind",
            "-P",
            "where/to/perform/search"
        ]
        let parser = ArgumentsParser(arguments: arguments)
        let runConfig = try! parser.parse()
        XCTAssertEqual(runConfig.symbolicLinkBehaviour, .doNotFollow)
    }

    func testExplicitFollowSymlinks() {
        let arguments: [String] = [
            "/path/to/uberfind",
            "-L",
            "where/to/perform/search"
        ]
        let parser = ArgumentsParser(arguments: arguments)
        let runConfig = try! parser.parse()
        XCTAssertEqual(runConfig.symbolicLinkBehaviour, .follow)
    }

    func testExcludePath() {
        let arguments: [String] = [
            "/path/to/uberfind",
            "--exclude-dir", "/path/to/exclude",
            "where/to/perform/search"
        ]
        let parser = ArgumentsParser(arguments: arguments)
        let runConfig = try! parser.parse()
        XCTAssertEqual(runConfig.pathToExclude, "/path/to/exclude")
    }

    func testValidLongestArgs() {
        let arguments: [String] = [
            "/path/to/uberfind",
            "-L",
            "--exclude-dir", "what/to/exclude",
            "where/to/perform/search",
            "-type", "l",
            "-name", "filename",
            "-rname", "pattern",
        ]
        let parser = ArgumentsParser(arguments: arguments)
        let runConfig = try! parser.parse()
        XCTAssertEqual(runConfig.symbolicLinkBehaviour, .follow)
        XCTAssertEqual(runConfig.pathToExclude, "what/to/exclude")
    }

    func testWithoutArgs() {
        let arguments: [String] = ["/path/to/uberfind"]
        let parser = ArgumentsParser(arguments: arguments)
        XCTAssertThrowsError(try parser.parse()) { error in
            XCTAssertEqual(error as? ArgumentsParser.ParseError, ArgumentsParser.ParseError.pathMissing)
        }
    }

    func testUnparsableArgsTreatedAsPaths() {
        let arguments: [String] = ["/path/to/uberfind", "-unknown", "-arg"]
        let parser = ArgumentsParser(arguments: arguments)
        XCTAssertThrowsError(try parser.parse()) { error in
            XCTAssertEqual(error as? ArgumentsParser.ParseError, ArgumentsParser.ParseError.moreThanSinglePathGiven)
        }
    }

    func testArgWithoutValue() {
        let arguments: [String] = ["/path/to/uberfind", "path", "-type"]
        let parser = ArgumentsParser(arguments: arguments)
        XCTAssertThrowsError(try parser.parse()) { error in
            XCTAssertEqual(
                error as? ArgumentsParser.ParseError,
                ArgumentsParser.ParseError.noArgument(atIndex: 3)
            )
        }
    }

    func testArgsWithoutPath() {
        let arguments: [String] = ["/path/to/uberfind", "-type", "d"]
        let parser = ArgumentsParser(arguments: arguments)
        XCTAssertThrowsError(try parser.parse()) { error in
            XCTAssertEqual(
                error as? ArgumentsParser.ParseError,
                ArgumentsParser.ParseError.pathMissing
            )
        }
    }
}
