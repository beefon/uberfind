//
//  String+PathTests.swift
//  FinderTests
//
//  Created by Vladislav Alexeev on 17.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation
import XCTest
@testable import FinderLibrary

class StringPathTests: XCTestCase {
    func testDotPrefix() {
        XCTAssertEqual(".".dotPrefix(), ".")
        XCTAssertEqual("./".dotPrefix(), ".")
        XCTAssertEqual("..".dotPrefix(), "..")
        XCTAssertEqual("../".dotPrefix(), "..")
        XCTAssertEqual("./hello".dotPrefix(), ".")
        XCTAssertEqual(".hello".dotPrefix(), nil)
        XCTAssertEqual("hello".dotPrefix(), nil)
        XCTAssertEqual("hello/.".dotPrefix(), nil)
    }
}
