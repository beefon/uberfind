//
//  FileNameTests.swift
//  FinderTests
//
//  Created by Vladislav Alexeev on 24.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation
import XCTest
@testable import FinderLibrary

class FileNameTests: XCTestCase {

    let delegate = CollectingFinderDelegate()

    func testMatchingFilename() {
        let testDir = FixtureManager.relativePathForFixture("FilesAndFolders")

        let config = try! RunConfigBuilder()
            .with(path: testDir)
            .with(mergedExpression: Expression(name: "root_file.txt"))
            .build()
        let finder = Finder(runConfig: config, delegate: delegate)
        finder.find()
        XCTAssertEqual(delegate.collectedOutputs, [testDir + "/root_file.txt"])
    }

    func testMatchingFilenameInsideFolder() {
        let testDir = FixtureManager.relativePathForFixture("FilesAndFolders")
        let config = try! RunConfigBuilder()
            .with(path: testDir)
            .with(mergedExpression: Expression(name: "deep_file.txt"))
            .build()
        let finder = Finder(runConfig: config, delegate: delegate)
        finder.find()
        XCTAssertEqual(delegate.collectedOutputs, [testDir + "/k/subfolder_in_k/deep_folder/deep_file.txt"])
    }

    func testMatchingGivenPattern() {
        let testDir = FixtureManager.relativePathForFixture("FilesAndFolders")
        let config = try! RunConfigBuilder()
            .with(path: testDir)
            .with(mergedExpression: Expression(rname: ".*root_file.txt"))
            .build()
        let finder = Finder(runConfig: config, delegate: delegate)
        finder.find()

        let expectedFiles = [
            testDir + "/another_root_file.txt",
            testDir + "/root_file.txt",
            testDir + "/links/link_to_root_file.txt",
            ]
        XCTAssertEqual(delegate.collectedOutputs, Set(expectedFiles))
    }
}
