//
//  FollowingSymlinkTests.swift
//  FinderTests
//
//  Created by Vladislav Alexeev on 24.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation
import XCTest
@testable import FinderLibrary

class FollowingSymlinkTests: XCTestCase {

    let delegate = CollectingFinderDelegate()

    func testFollowingSymlinks()  {
        let testDir = FixtureManager.relativePathForFixture("FilesAndFolders")
        let config = try! RunConfigBuilder()
            .with(path: testDir)
            .with(symbolicLinkBehaviour: .follow)
            .build()
        let finder = Finder(runConfig: config, delegate: delegate)
        finder.find()

        let expectedFiles = [
            testDir + "/another_root_file.txt",
            testDir + "/root_file.txt",
            testDir + "/k",
            testDir + "/k/another_file_in_k.txt",
            testDir + "/k/file_in_k.txt",
            testDir + "/k/subfolder_in_k",
            testDir + "/k/subfolder_in_k/deep_folder",
            testDir + "/k/subfolder_in_k/deep_folder/deep_file.txt",
            testDir + "/k/subfolder_in_k/file_in_subfolder_in_k.txt",
            testDir + "/some_folder",
            testDir + "/some_folder/file_in_some_folder.txt",
            testDir + "/links",
            testDir + "/root_file.txt",     // resolved symlink
            testDir + "/links/link_to_non_existing_file.txt",       // points to non-existing file
            ]
        XCTAssertEqual(delegate.collectedOutputs, Set(expectedFiles))
    }

    func testSpecifyingSymlinkTypeOutputsOnlySymlinksThatPointToNonExistingFiles() {
        let testDir = FixtureManager.relativePathForFixture("FilesAndFolders")
        let config = try! RunConfigBuilder()
            .with(path: testDir)
            .with(symbolicLinkBehaviour: .follow)
            .with(mergedExpression: Expression(fileType: Expression.FileType.symbolicLink))
            .build()
        let finder = Finder(runConfig: config, delegate: delegate)
        finder.find()

        let expectedFiles = [
            testDir + "/links/link_to_non_existing_file.txt",
        ]
        XCTAssertEqual(delegate.collectedOutputs, Set(expectedFiles))
    }
}
