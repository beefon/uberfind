//
//  UberFindTests.swift
//  UberFindTests
//
//  Created by Vladislav Alexeev on 15.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import XCTest
@testable import FinderLibrary

class BasicTests: XCTestCase {

    let delegate = CollectingFinderDelegate()

    func testBasicEnumeration() {
        let testDir = FixtureManager.absolutePathForFixture("OnlyFiles")
        let finder = Finder(
            runConfig: try! RunConfigBuilder().with(path: testDir).build(),
            delegate: delegate)
        finder.find()
        let actualFiles = try! FileManager.default.contentsOfDirectory(atPath: testDir)

        XCTAssertTrue(delegate.collectedOutputs.count == actualFiles.count)
    }

    func testEnumerationWithAbsolutePathReturnsAbsolutePaths() {
        let testDir = FixtureManager.absolutePathForFixture("OnlyFiles")
        let finder = Finder(
            runConfig: try! RunConfigBuilder().with(path: testDir).build(),
            delegate: delegate)
        finder.find()

        let expectedPaths = try! FileManager.default
            .contentsOfDirectory(atPath: testDir)
            .map { testDir.appending("/\($0)") }

        for path in delegate.collectedOutputs {
            XCTAssert(path.hasPrefix("/"))
        }

        XCTAssertEqual(delegate.collectedOutputs, Set(expectedPaths))
    }

    func testEnumerationWithRelativePathReturnsRelativePaths() {
        let testDir = FixtureManager.relativePathForFixture("OnlyFiles")
        let finder = Finder(
            runConfig: try! RunConfigBuilder().with(path: testDir).build(),
            delegate: delegate)
        finder.find()

        let expectedPaths = try! FileManager.default.contentsOfDirectory(atPath: testDir)
            .map { testDir + "/" + $0 }

        for path in delegate.collectedOutputs {
            XCTAssertFalse(path.hasPrefix("/"))
        }

        XCTAssertEqual(delegate.collectedOutputs, Set(expectedPaths))
    }

    func testEnumerationWithRelativePathThatStartsWithDotSlashReturnsRelativePathsWithDotSlash() {
        let testDir = "./" + FixtureManager.relativePathForFixture("OnlyFiles")
        let finder = Finder(
            runConfig: try! RunConfigBuilder().with(path: testDir).build(),
            delegate: delegate)
        finder.find()

        let expectedPaths = try! FileManager.default.contentsOfDirectory(atPath: testDir)
            .map { testDir + "/" + $0 }

        XCTAssertEqual(delegate.collectedOutputs, Set(expectedPaths))
    }

    func testEnumerationWithRelativePathThatStartsWithDoubleDotReturnsRelativePathsWithDoubleDot() {
        let lastComponent = FileManager.default.currentDirectoryPath.pathComponents.last!
        let testDir = "../\(lastComponent)/" + FixtureManager.relativePathForFixture("OnlyFiles")
        let finder = Finder(
            runConfig: try! RunConfigBuilder().with(path: testDir).build(),
            delegate: delegate)
        finder.find()

        let expectedPaths = try! FileManager.default.contentsOfDirectory(atPath: testDir)
            .map { testDir + "/" + $00 }

        XCTAssertEqual(delegate.collectedOutputs, Set(expectedPaths))
    }
}
