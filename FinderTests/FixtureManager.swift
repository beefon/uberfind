//
//  FixtureManager.swift
//  FinderTests
//
//  Created by Vladislav Alexeev on 16.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation
import XCTest

final class FixtureManager {
    private init() {}

    static func absolutePathForFixture(_ name: String, file: StaticString = #file, line: UInt = #line) -> String {
        let testBundle = Bundle(for: FixtureManager.self)
        guard let testDir = testBundle.path(forResource: name, ofType: "bundle") else {
            XCTFail("Unable to find fixture \(name).", file: file, line: line)
            return ""
        }
        return testDir
    }

    static func relativePathForFixture(_ name: String, file: StaticString = #file, line: UInt = #line) -> String {
        let absolutePath = absolutePathForFixture(name, file: file, line: line)
        let anchorPath = FileManager.default.currentDirectoryPath
        guard let relativePath = absolutePath.stringWithPathRelativeTo(anchorPath: anchorPath) else {
            XCTFail("Unable to find make \(absolutePath) relative to \(anchorPath).", file: file, line: line)
            return ""
        }
        return relativePath
    }
}
