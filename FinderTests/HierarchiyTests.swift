//
//  EnumeratingHierarchiyTests.swift
//  FinderTests
//
//  Created by Vladislav Alexeev on 22.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation

import XCTest
@testable import FinderLibrary

class HierarchiyTests: XCTestCase {

    let delegate = CollectingFinderDelegate()

    func testEnumeratinFolderHierarchy() {
        let testDir = FixtureManager.relativePathForFixture("FilesAndFolders")
        let finder = Finder(
            runConfig: try! RunConfigBuilder().with(path: testDir).build(),
            delegate: delegate)
        finder.find()

        let expectedFiles = [
            testDir + "/another_root_file.txt",
            testDir + "/root_file.txt",
            testDir + "/k",
            testDir + "/k/another_file_in_k.txt",
            testDir + "/k/file_in_k.txt",
            testDir + "/k/subfolder_in_k",
            testDir + "/k/subfolder_in_k/deep_folder",
            testDir + "/k/subfolder_in_k/deep_folder/deep_file.txt",
            testDir + "/k/subfolder_in_k/file_in_subfolder_in_k.txt",
            testDir + "/some_folder",
            testDir + "/some_folder/file_in_some_folder.txt",
            testDir + "/links",
            testDir + "/links/link_to_root_file.txt",
            testDir + "/links/link_to_non_existing_file.txt",
            testDir + "/links/link_to_some_folder.txt",
            ]

        XCTAssertEqual(delegate.collectedOutputs, Set(expectedFiles))
    }

    func testEnumeratingOnlyFiles() {
        let testDir = FixtureManager.relativePathForFixture("FilesAndFolders")
        let config = try! RunConfigBuilder()
            .with(path: testDir)
            .with(mergedExpression: Expression(fileType: Expression.FileType.regularFile))
            .build()
        let finder = Finder(runConfig: config, delegate: delegate)
        finder.find()

        let expectedFiles = [
            testDir + "/another_root_file.txt",
            testDir + "/root_file.txt",
            testDir + "/k/another_file_in_k.txt",
            testDir + "/k/file_in_k.txt",
            testDir + "/k/subfolder_in_k/deep_folder/deep_file.txt",
            testDir + "/k/subfolder_in_k/file_in_subfolder_in_k.txt",
            testDir + "/some_folder/file_in_some_folder.txt",
            ]

        XCTAssertEqual(delegate.collectedOutputs, Set(expectedFiles))
    }

    func testEnumeratingOnlyFolders() {
        let testDir = FixtureManager.relativePathForFixture("FilesAndFolders")
        let config = try! RunConfigBuilder()
            .with(path: testDir)
            .with(mergedExpression: Expression(fileType: Expression.FileType.directory))
            .build()
        let finder = Finder(runConfig: config, delegate: delegate)
        finder.find()

        let expectedFiles = [
            testDir + "/k",
            testDir + "/k/subfolder_in_k",
            testDir + "/k/subfolder_in_k/deep_folder",
            testDir + "/some_folder",
            testDir + "/links",
            ]

        XCTAssertEqual(delegate.collectedOutputs, Set(expectedFiles))
    }

    func testEnumeratingOnlySymlinks() {
        let testDir = FixtureManager.relativePathForFixture("FilesAndFolders")
        let config = try! RunConfigBuilder()
            .with(path: testDir)
            .with(mergedExpression: Expression(fileType: Expression.FileType.symbolicLink))
            .build()
        let finder = Finder(runConfig: config, delegate: delegate)
        finder.find()

        let expectedFiles = [
            testDir + "/links/link_to_root_file.txt",
            testDir + "/links/link_to_non_existing_file.txt",
            testDir + "/links/link_to_some_folder.txt",
            ]

        XCTAssertEqual(delegate.collectedOutputs, Set(expectedFiles))
    }
}
