//
//  CollectingFinderDelegate.swift
//  FinderTests
//
//  Created by Vladislav Alexeev on 26.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation
@testable import FinderLibrary

class CollectingFinderDelegate: FinderDelegate {
    var collectedOutputs = Set<String>()
    func didFindPath(_ path: String) {
        collectedOutputs.insert(path)
    }
}
