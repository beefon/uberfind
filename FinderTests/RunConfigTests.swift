//
//  RunConfigTests.swift
//  FinderTests
//
//  Created by Vladislav Alexeev on 17.03.2018.
//  Copyright © 2018 beefon. All rights reserved.
//

import Foundation
import XCTest
@testable import FinderLibrary

class RunConfigTests: XCTestCase {
    func testPathOutputStyle()  {
        XCTAssertTrue(try! RunConfigBuilder(path: ".").build().shouldOutputRelativePath())
        XCTAssertTrue(try! RunConfigBuilder(path: "Something").build().shouldOutputRelativePath())
        XCTAssertFalse(try! RunConfigBuilder(path: "/").build().shouldOutputRelativePath())
        XCTAssertFalse(try! RunConfigBuilder(path: "~/").build().shouldOutputRelativePath())
    }
}
